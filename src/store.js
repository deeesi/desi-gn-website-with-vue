export default {
	//Object
	state: {
		// Array
		referenzen: [
			{
				id: '1',
				headline: 'Flying Tangram',
				desc: 'Hier eine lustige Beschreibung',
				img: 'assets/img/desi-avatar.png'
			},
			{
				id: '2',
				title: 'Milchreis',
				category: '2',
				tags: ['1', '2', '3']
			},
			{
				id: '3',
				title: 'Nudeln mit Soße',
				category: '1',
				tags: ['1', '2', '3']
			},
			{
				id: '4',
				title: 'Sauerkrautauflauf',
				category: '1',
				tags: ['4']
			},
			{
				id: '5',
				title: 'Apfelkuchen',
				category: '3',
				tags: ['2', '4', '5']
			},
			{
				id: '6',
				title: 'Gemüselasagne',
				category: '1',
				tags: ['2', '4', '5']
			}
		]
	}
};
